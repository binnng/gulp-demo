var $, API_PATH, ApkURL, AwardComb, AwardStatusComb, CLICK, ClickTrace, Cookie, DOC, HOST, HREF, IsAndroid, IsFormLowerAndroid, IsFromClient, IsFromOldClient, IsTouch, LOC, SEARCH, Storage, Swipe, SysVersion, UA, URL, WIN;

WIN = window;

DOC = document;

LOC = location;

HREF = LOC.href;

HOST = LOC.host;

SEARCH = LOC.search;

UA = WIN.navigator.userAgent;

Cookie = WIN['Cookie'];

Storage = WIN['Storage'];

URL = WIN['URL'];

Swipe = WIN['Swipe'];

ClickTrace = WIN['ClickTrace'];

$ = WIN['Zepto'];

IsTouch = 'ontouchstart' in WIN;

IsAndroid = /Android|HTC/i.test(UA) || !!(WIN.navigator['platform'] + '').match(/Linux/i);

CLICK = IsTouch ? 'touchstart' : 'mousedown';

API_PATH = (HOST.match(/10.2/) ? '' : 'http://m.tv.sohu.com') + '/h5/avt/';

ApkURL = 'http://upgrade.m.tv.sohu.com/channels/hdv/680/3.5/SohuTV_3.5_680_201311221739.apk?t=1';

IsFromClient = false;

IsFromOldClient = false;

IsFormLowerAndroid = false;

if (IsAndroid) {
  SysVersion = UA.match(/Android(?:[\/\s*]([0-9\._]+))?/i);
  SysVersion = SysVersion ? SysVersion[1].replace(/\./g, '') : 0;
  if (SysVersion < 240) {
    IsFormLowerAndroid = true;
  }
}

AwardComb = AwardStatusComb = "";
